﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Authors:        Shoval  Eliav
///                 Nir     Spindel
/// File name:      Costumer.cs
/// 
/// This file for Class Costumer
/// </summary>
namespace Store
{
    class Costumer
    {
        /// <summary>
        /// Attributes of Costumer with Getters & Setters
        /// ------------------------------------------------------
        /// @param ID           :   int         -   ID of Costumer
        /// @param FirstName    :   string      -   Firstname of Costumer
        /// @param LastName     :   string      -   LastName of Costumer
        /// @param PhoneNumber  :   string      -   PhoneNumber of Costumer
        /// @param StoreName    :   string      -   StoreName of Costumer
        /// @param StoreLocation:   string      -   StoreLocation of Costumer
        /// </summary>
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StoreName { get; set; }
        public string City { get; set; }

        /// <summary>
        /// Constructor of Costumer
        /// ---------------------------------
        /// </summary>
        public Costumer()
        {
            ID = -1;
            FirstName = string.Empty;
            LastName = string.Empty;
            PhoneNumber = string.Empty;
            StoreName = string.Empty;
            City = string.Empty;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} ", this.FirstName, this.LastName);
        }

    }
}
