﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Authors:        Shoval  Eliav
///                 Nir     Spindel
/// File name:      Provider.cs
/// 
/// This file for Class Provider
/// </summary>
namespace Store
{
    class Provider
    {
        /// <summary>
        /// Attributes of Provider with Getters & Setters
        /// ------------------------------------------------------
        /// @param ID           :   int         -   ID of provider
        /// @param FirstName    :   string      -   Firstname of provider
        /// @param LastName     :   string      -   LastName of provider
        /// @param PhoneNumber  :   string      -   PhoneNumber of provider
        /// @param Category     :   string      -   Category of provider
        /// </summary>
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Category { get; set; }
        public string Amount { get; set; }

        /// <summary>
        /// Constructor of Provider
        /// ---------------------------------
        /// </summary>
        public Provider()
        {
            ID = -1;
            FirstName = string.Empty;
            LastName = string.Empty;
            PhoneNumber = string.Empty;
            Category = string.Empty;
            Amount = string.Empty;
        }



        //shoval to fix
        public override string ToString()
        {
            return string.Format("{0} {1} ", this.FirstName, this.LastName);
        }
    }
}
