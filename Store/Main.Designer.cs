﻿namespace Store
{
    partial class MainProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userTabControl = new System.Windows.Forms.TabControl();
            this.tabHome = new System.Windows.Forms.TabPage();
            this.lbMainPage = new System.Windows.Forms.Label();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.cbSearchOrdersByCostumer = new System.Windows.Forms.ComboBox();
            this.cbProductSearchByprovider = new System.Windows.Forms.ComboBox();
            this.tabOrder = new System.Windows.Forms.TabPage();
            this.cbCostumerOrder = new System.Windows.Forms.ComboBox();
            this.cbProductOrder = new System.Windows.Forms.ComboBox();
            this.cbProviderOrder = new System.Windows.Forms.ComboBox();
            this.tabReport = new System.Windows.Forms.TabPage();
            this.cbProductsReport = new System.Windows.Forms.ComboBox();
            this.cbProvidersReport = new System.Windows.Forms.ComboBox();
            this.btnProductsReport = new System.Windows.Forms.Button();
            this.btnProvidersReport = new System.Windows.Forms.Button();
            this.btnCostumersReport = new System.Windows.Forms.Button();
            this.cbCostumerReport = new System.Windows.Forms.ComboBox();
            this.tabUpdate = new System.Windows.Forms.TabPage();
            this.cProduct = new System.Windows.Forms.TextBox();
            this.Nproduct = new System.Windows.Forms.TextBox();
            this.priceProduct = new System.Windows.Forms.TextBox();
            this.aProduct = new System.Windows.Forms.TextBox();
            this.sProduct = new System.Windows.Forms.TextBox();
            this.UpdateProductBtn = new System.Windows.Forms.Button();
            this.UpdateProviderBtn = new System.Windows.Forms.Button();
            this.amProviderUpdate = new System.Windows.Forms.TextBox();
            this.ctProviderUpdate = new System.Windows.Forms.TextBox();
            this.pnProviderUpdate = new System.Windows.Forms.TextBox();
            this.lnProviderUpdate = new System.Windows.Forms.TextBox();
            this.fnProviderUpdate = new System.Windows.Forms.TextBox();
            this.snCostumerUpdate = new System.Windows.Forms.TextBox();
            this.UpdateCostumerBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cityCostumerUpdate = new System.Windows.Forms.TextBox();
            this.pnCostumerUpdate = new System.Windows.Forms.TextBox();
            this.lnCostumerUpdate = new System.Windows.Forms.TextBox();
            this.fnCostumerUpdate = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.removeProduct = new System.Windows.Forms.Button();
            this.removeProvider = new System.Windows.Forms.Button();
            this.removeCostumer = new System.Windows.Forms.Button();
            this.productsCb = new System.Windows.Forms.ComboBox();
            this.providersCb = new System.Windows.Forms.ComboBox();
            this.costumerCb = new System.Windows.Forms.ComboBox();
            this.deleteProductBtn = new System.Windows.Forms.Button();
            this.deleteproviderBtn = new System.Windows.Forms.Button();
            this.deleteCostumerBtn = new System.Windows.Forms.Button();
            this.userTabControl.SuspendLayout();
            this.tabHome.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.tabOrder.SuspendLayout();
            this.tabReport.SuspendLayout();
            this.tabUpdate.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // userTabControl
            // 
            this.userTabControl.Controls.Add(this.tabHome);
            this.userTabControl.Controls.Add(this.tabSearch);
            this.userTabControl.Controls.Add(this.tabOrder);
            this.userTabControl.Controls.Add(this.tabReport);
            this.userTabControl.Controls.Add(this.tabUpdate);
            this.userTabControl.Controls.Add(this.tabPage1);
            this.userTabControl.Location = new System.Drawing.Point(0, 0);
            this.userTabControl.Name = "userTabControl";
            this.userTabControl.SelectedIndex = 0;
            this.userTabControl.Size = new System.Drawing.Size(805, 448);
            this.userTabControl.TabIndex = 0;
            // 
            // tabHome
            // 
            this.tabHome.BackColor = System.Drawing.Color.SkyBlue;
            this.tabHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabHome.Controls.Add(this.lbMainPage);
            this.tabHome.Location = new System.Drawing.Point(4, 22);
            this.tabHome.Name = "tabHome";
            this.tabHome.Size = new System.Drawing.Size(797, 422);
            this.tabHome.TabIndex = 0;
            this.tabHome.Text = "Home";
            // 
            // lbMainPage
            // 
            this.lbMainPage.AutoSize = true;
            this.lbMainPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainPage.Location = new System.Drawing.Point(159, 33);
            this.lbMainPage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbMainPage.Name = "lbMainPage";
            this.lbMainPage.Size = new System.Drawing.Size(502, 351);
            this.lbMainPage.TabIndex = 0;
            this.lbMainPage.Text = "\r\nSoftware\'s name: Clothing Store\r\nBy: Nir Spindel & Shoval Eliav\r\nDescription:\r\n" +
    "1. Search\r\n2. Order\r\n3. Report\r\n4. Update\r\n\r\n";
            // 
            // tabSearch
            // 
            this.tabSearch.BackColor = System.Drawing.Color.SkyBlue;
            this.tabSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabSearch.Controls.Add(this.cbSearchOrdersByCostumer);
            this.tabSearch.Controls.Add(this.cbProductSearchByprovider);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Size = new System.Drawing.Size(797, 422);
            this.tabSearch.TabIndex = 1;
            this.tabSearch.Text = "Search";
            // 
            // cbSearchOrdersByCostumer
            // 
            this.cbSearchOrdersByCostumer.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbSearchOrdersByCostumer.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSearchOrdersByCostumer.FormattingEnabled = true;
            this.cbSearchOrdersByCostumer.Location = new System.Drawing.Point(485, 31);
            this.cbSearchOrdersByCostumer.Name = "cbSearchOrdersByCostumer";
            this.cbSearchOrdersByCostumer.Size = new System.Drawing.Size(214, 39);
            this.cbSearchOrdersByCostumer.TabIndex = 2;
            this.cbSearchOrdersByCostumer.Text = "Select provider";
            // 
            // cbProductSearchByprovider
            // 
            this.cbProductSearchByprovider.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbProductSearchByprovider.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProductSearchByprovider.FormattingEnabled = true;
            this.cbProductSearchByprovider.Location = new System.Drawing.Point(140, 31);
            this.cbProductSearchByprovider.Name = "cbProductSearchByprovider";
            this.cbProductSearchByprovider.Size = new System.Drawing.Size(214, 39);
            this.cbProductSearchByprovider.TabIndex = 1;
            this.cbProductSearchByprovider.Text = "Select costumer";
            // 
            // tabOrder
            // 
            this.tabOrder.BackColor = System.Drawing.Color.SkyBlue;
            this.tabOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabOrder.Controls.Add(this.cbCostumerOrder);
            this.tabOrder.Controls.Add(this.cbProductOrder);
            this.tabOrder.Controls.Add(this.cbProviderOrder);
            this.tabOrder.Location = new System.Drawing.Point(4, 22);
            this.tabOrder.Name = "tabOrder";
            this.tabOrder.Size = new System.Drawing.Size(797, 422);
            this.tabOrder.TabIndex = 2;
            this.tabOrder.Text = "Order";
            // 
            // cbCostumerOrder
            // 
            this.cbCostumerOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbCostumerOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCostumerOrder.FormattingEnabled = true;
            this.cbCostumerOrder.Location = new System.Drawing.Point(532, 31);
            this.cbCostumerOrder.Name = "cbCostumerOrder";
            this.cbCostumerOrder.Size = new System.Drawing.Size(248, 39);
            this.cbCostumerOrder.TabIndex = 4;
            this.cbCostumerOrder.Text = "Order submitted to";
            // 
            // cbProductOrder
            // 
            this.cbProductOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbProductOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProductOrder.FormattingEnabled = true;
            this.cbProductOrder.Location = new System.Drawing.Point(272, 31);
            this.cbProductOrder.Name = "cbProductOrder";
            this.cbProductOrder.Size = new System.Drawing.Size(214, 39);
            this.cbProductOrder.TabIndex = 3;
            this.cbProductOrder.Text = "Choose product";
            this.cbProductOrder.SelectedIndexChanged += new System.EventHandler(this.cbProductOrder_SelectedIndexChanged);
            // 
            // cbProviderOrder
            // 
            this.cbProviderOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbProviderOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProviderOrder.FormattingEnabled = true;
            this.cbProviderOrder.Location = new System.Drawing.Point(12, 31);
            this.cbProviderOrder.Name = "cbProviderOrder";
            this.cbProviderOrder.Size = new System.Drawing.Size(224, 39);
            this.cbProviderOrder.TabIndex = 2;
            this.cbProviderOrder.Text = "Choose provider";
            // 
            // tabReport
            // 
            this.tabReport.BackColor = System.Drawing.Color.SkyBlue;
            this.tabReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabReport.Controls.Add(this.cbProductsReport);
            this.tabReport.Controls.Add(this.cbProvidersReport);
            this.tabReport.Controls.Add(this.btnProductsReport);
            this.tabReport.Controls.Add(this.btnProvidersReport);
            this.tabReport.Controls.Add(this.btnCostumersReport);
            this.tabReport.Controls.Add(this.cbCostumerReport);
            this.tabReport.Location = new System.Drawing.Point(4, 22);
            this.tabReport.Name = "tabReport";
            this.tabReport.Size = new System.Drawing.Size(797, 422);
            this.tabReport.TabIndex = 3;
            this.tabReport.Text = "Report";
            // 
            // cbProductsReport
            // 
            this.cbProductsReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbProductsReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProductsReport.FormattingEnabled = true;
            this.cbProductsReport.Location = new System.Drawing.Point(572, 116);
            this.cbProductsReport.Name = "cbProductsReport";
            this.cbProductsReport.Size = new System.Drawing.Size(174, 24);
            this.cbProductsReport.TabIndex = 8;
            this.cbProductsReport.Text = "Select product";
            this.cbProductsReport.Visible = false;
            // 
            // cbProvidersReport
            // 
            this.cbProvidersReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbProvidersReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProvidersReport.FormattingEnabled = true;
            this.cbProvidersReport.Location = new System.Drawing.Point(302, 116);
            this.cbProvidersReport.Name = "cbProvidersReport";
            this.cbProvidersReport.Size = new System.Drawing.Size(174, 24);
            this.cbProvidersReport.TabIndex = 7;
            this.cbProvidersReport.Text = "Select provider";
            this.cbProvidersReport.Visible = false;
            this.cbProvidersReport.SelectedIndexChanged += new System.EventHandler(this.cbProvidersReport_SelectedIndexChanged);
            // 
            // btnProductsReport
            // 
            this.btnProductsReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnProductsReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductsReport.Location = new System.Drawing.Point(572, 16);
            this.btnProductsReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnProductsReport.Name = "btnProductsReport";
            this.btnProductsReport.Size = new System.Drawing.Size(172, 63);
            this.btnProductsReport.TabIndex = 6;
            this.btnProductsReport.Text = "Export products";
            this.btnProductsReport.UseVisualStyleBackColor = false;
            this.btnProductsReport.Click += new System.EventHandler(this.btnProductsReport_Click);
            // 
            // btnProvidersReport
            // 
            this.btnProvidersReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnProvidersReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProvidersReport.Location = new System.Drawing.Point(302, 16);
            this.btnProvidersReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnProvidersReport.Name = "btnProvidersReport";
            this.btnProvidersReport.Size = new System.Drawing.Size(172, 63);
            this.btnProvidersReport.TabIndex = 5;
            this.btnProvidersReport.Text = "Export providers";
            this.btnProvidersReport.UseVisualStyleBackColor = false;
            this.btnProvidersReport.Click += new System.EventHandler(this.btnProvidersReport_Click);
            // 
            // btnCostumersReport
            // 
            this.btnCostumersReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCostumersReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCostumersReport.Location = new System.Drawing.Point(38, 16);
            this.btnCostumersReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnCostumersReport.Name = "btnCostumersReport";
            this.btnCostumersReport.Size = new System.Drawing.Size(172, 63);
            this.btnCostumersReport.TabIndex = 4;
            this.btnCostumersReport.Text = "Export costumers";
            this.btnCostumersReport.UseVisualStyleBackColor = false;
            this.btnCostumersReport.Click += new System.EventHandler(this.btnCostumersReport_Click);
            // 
            // cbCostumerReport
            // 
            this.cbCostumerReport.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbCostumerReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCostumerReport.FormattingEnabled = true;
            this.cbCostumerReport.Location = new System.Drawing.Point(38, 116);
            this.cbCostumerReport.Name = "cbCostumerReport";
            this.cbCostumerReport.Size = new System.Drawing.Size(174, 24);
            this.cbCostumerReport.TabIndex = 3;
            this.cbCostumerReport.Text = "Select costumer";
            this.cbCostumerReport.Visible = false;
            this.cbCostumerReport.SelectedIndexChanged += new System.EventHandler(this.cbCostumerReport_SelectedIndexChanged);
            // 
            // tabUpdate
            // 
            this.tabUpdate.BackColor = System.Drawing.Color.SkyBlue;
            this.tabUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabUpdate.Controls.Add(this.cProduct);
            this.tabUpdate.Controls.Add(this.Nproduct);
            this.tabUpdate.Controls.Add(this.priceProduct);
            this.tabUpdate.Controls.Add(this.aProduct);
            this.tabUpdate.Controls.Add(this.sProduct);
            this.tabUpdate.Controls.Add(this.UpdateProductBtn);
            this.tabUpdate.Controls.Add(this.UpdateProviderBtn);
            this.tabUpdate.Controls.Add(this.amProviderUpdate);
            this.tabUpdate.Controls.Add(this.ctProviderUpdate);
            this.tabUpdate.Controls.Add(this.pnProviderUpdate);
            this.tabUpdate.Controls.Add(this.lnProviderUpdate);
            this.tabUpdate.Controls.Add(this.fnProviderUpdate);
            this.tabUpdate.Controls.Add(this.snCostumerUpdate);
            this.tabUpdate.Controls.Add(this.UpdateCostumerBtn);
            this.tabUpdate.Controls.Add(this.label1);
            this.tabUpdate.Controls.Add(this.cityCostumerUpdate);
            this.tabUpdate.Controls.Add(this.pnCostumerUpdate);
            this.tabUpdate.Controls.Add(this.lnCostumerUpdate);
            this.tabUpdate.Controls.Add(this.fnCostumerUpdate);
            this.tabUpdate.Location = new System.Drawing.Point(4, 22);
            this.tabUpdate.Name = "tabUpdate";
            this.tabUpdate.Size = new System.Drawing.Size(797, 422);
            this.tabUpdate.TabIndex = 4;
            this.tabUpdate.Text = "Update";
            this.tabUpdate.Click += new System.EventHandler(this.tabUpdate_Click);
            // 
            // cProduct
            // 
            this.cProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cProduct.Location = new System.Drawing.Point(598, 211);
            this.cProduct.Name = "cProduct";
            this.cProduct.Size = new System.Drawing.Size(174, 23);
            this.cProduct.TabIndex = 39;
            this.cProduct.Text = "Category";
            // 
            // Nproduct
            // 
            this.Nproduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nproduct.Location = new System.Drawing.Point(598, 124);
            this.Nproduct.Name = "Nproduct";
            this.Nproduct.Size = new System.Drawing.Size(174, 23);
            this.Nproduct.TabIndex = 38;
            this.Nproduct.Text = "Name";
            // 
            // priceProduct
            // 
            this.priceProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceProduct.Location = new System.Drawing.Point(598, 292);
            this.priceProduct.Name = "priceProduct";
            this.priceProduct.Size = new System.Drawing.Size(174, 23);
            this.priceProduct.TabIndex = 32;
            this.priceProduct.Text = "Price Per Unit";
            // 
            // aProduct
            // 
            this.aProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aProduct.Location = new System.Drawing.Point(598, 252);
            this.aProduct.Name = "aProduct";
            this.aProduct.Size = new System.Drawing.Size(174, 23);
            this.aProduct.TabIndex = 31;
            this.aProduct.Text = "Amount";
            // 
            // sProduct
            // 
            this.sProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sProduct.Location = new System.Drawing.Point(598, 167);
            this.sProduct.Name = "sProduct";
            this.sProduct.Size = new System.Drawing.Size(174, 23);
            this.sProduct.TabIndex = 29;
            this.sProduct.Text = "Size";
            // 
            // UpdateProductBtn
            // 
            this.UpdateProductBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateProductBtn.Location = new System.Drawing.Point(598, 37);
            this.UpdateProductBtn.Name = "UpdateProductBtn";
            this.UpdateProductBtn.Size = new System.Drawing.Size(172, 63);
            this.UpdateProductBtn.TabIndex = 27;
            this.UpdateProductBtn.Text = "add product";
            this.UpdateProductBtn.UseVisualStyleBackColor = true;
            this.UpdateProductBtn.Click += new System.EventHandler(this.UpdateProductBtn_Click);
            // 
            // UpdateProviderBtn
            // 
            this.UpdateProviderBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateProviderBtn.Location = new System.Drawing.Point(318, 37);
            this.UpdateProviderBtn.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateProviderBtn.Name = "UpdateProviderBtn";
            this.UpdateProviderBtn.Size = new System.Drawing.Size(172, 63);
            this.UpdateProviderBtn.TabIndex = 26;
            this.UpdateProviderBtn.Text = "Add provider";
            this.UpdateProviderBtn.UseVisualStyleBackColor = true;
            this.UpdateProviderBtn.Click += new System.EventHandler(this.UpdateProviderBtn_Click);
            // 
            // amProviderUpdate
            // 
            this.amProviderUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amProviderUpdate.Location = new System.Drawing.Point(318, 292);
            this.amProviderUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.amProviderUpdate.Name = "amProviderUpdate";
            this.amProviderUpdate.Size = new System.Drawing.Size(174, 23);
            this.amProviderUpdate.TabIndex = 25;
            this.amProviderUpdate.Text = "Amount";
            // 
            // ctProviderUpdate
            // 
            this.ctProviderUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctProviderUpdate.Location = new System.Drawing.Point(318, 252);
            this.ctProviderUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.ctProviderUpdate.Name = "ctProviderUpdate";
            this.ctProviderUpdate.Size = new System.Drawing.Size(174, 23);
            this.ctProviderUpdate.TabIndex = 24;
            this.ctProviderUpdate.Text = "Category";
            // 
            // pnProviderUpdate
            // 
            this.pnProviderUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnProviderUpdate.Location = new System.Drawing.Point(318, 211);
            this.pnProviderUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.pnProviderUpdate.Name = "pnProviderUpdate";
            this.pnProviderUpdate.Size = new System.Drawing.Size(174, 23);
            this.pnProviderUpdate.TabIndex = 23;
            this.pnProviderUpdate.Text = "Phone Number";
            // 
            // lnProviderUpdate
            // 
            this.lnProviderUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnProviderUpdate.Location = new System.Drawing.Point(318, 167);
            this.lnProviderUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.lnProviderUpdate.Name = "lnProviderUpdate";
            this.lnProviderUpdate.Size = new System.Drawing.Size(174, 23);
            this.lnProviderUpdate.TabIndex = 22;
            this.lnProviderUpdate.Text = "Last Name";
            // 
            // fnProviderUpdate
            // 
            this.fnProviderUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnProviderUpdate.Location = new System.Drawing.Point(318, 124);
            this.fnProviderUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.fnProviderUpdate.Name = "fnProviderUpdate";
            this.fnProviderUpdate.Size = new System.Drawing.Size(174, 23);
            this.fnProviderUpdate.TabIndex = 21;
            this.fnProviderUpdate.Text = "First Name";
            // 
            // snCostumerUpdate
            // 
            this.snCostumerUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.snCostumerUpdate.Location = new System.Drawing.Point(26, 252);
            this.snCostumerUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.snCostumerUpdate.Name = "snCostumerUpdate";
            this.snCostumerUpdate.Size = new System.Drawing.Size(174, 23);
            this.snCostumerUpdate.TabIndex = 15;
            this.snCostumerUpdate.Text = "Store Name";
            this.snCostumerUpdate.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // UpdateCostumerBtn
            // 
            this.UpdateCostumerBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateCostumerBtn.Location = new System.Drawing.Point(26, 37);
            this.UpdateCostumerBtn.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateCostumerBtn.Name = "UpdateCostumerBtn";
            this.UpdateCostumerBtn.Size = new System.Drawing.Size(172, 63);
            this.UpdateCostumerBtn.TabIndex = 12;
            this.UpdateCostumerBtn.Text = "add costumer";
            this.UpdateCostumerBtn.UseVisualStyleBackColor = true;
            this.UpdateCostumerBtn.Click += new System.EventHandler(this.UpdateCostumerBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 109);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 8;
            // 
            // cityCostumerUpdate
            // 
            this.cityCostumerUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityCostumerUpdate.Location = new System.Drawing.Point(26, 292);
            this.cityCostumerUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.cityCostumerUpdate.Name = "cityCostumerUpdate";
            this.cityCostumerUpdate.Size = new System.Drawing.Size(174, 23);
            this.cityCostumerUpdate.TabIndex = 7;
            this.cityCostumerUpdate.Text = "City";
            // 
            // pnCostumerUpdate
            // 
            this.pnCostumerUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnCostumerUpdate.Location = new System.Drawing.Point(26, 211);
            this.pnCostumerUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.pnCostumerUpdate.Name = "pnCostumerUpdate";
            this.pnCostumerUpdate.Size = new System.Drawing.Size(174, 23);
            this.pnCostumerUpdate.TabIndex = 6;
            this.pnCostumerUpdate.Text = "Phone Number";
            this.pnCostumerUpdate.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // lnCostumerUpdate
            // 
            this.lnCostumerUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnCostumerUpdate.Location = new System.Drawing.Point(26, 167);
            this.lnCostumerUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.lnCostumerUpdate.Name = "lnCostumerUpdate";
            this.lnCostumerUpdate.Size = new System.Drawing.Size(174, 23);
            this.lnCostumerUpdate.TabIndex = 4;
            this.lnCostumerUpdate.Text = "Last Name";
            // 
            // fnCostumerUpdate
            // 
            this.fnCostumerUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fnCostumerUpdate.Location = new System.Drawing.Point(26, 124);
            this.fnCostumerUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.fnCostumerUpdate.Name = "fnCostumerUpdate";
            this.fnCostumerUpdate.Size = new System.Drawing.Size(174, 23);
            this.fnCostumerUpdate.TabIndex = 3;
            this.fnCostumerUpdate.Text = "First Name";
            this.fnCostumerUpdate.TextChanged += new System.EventHandler(this.fnCostumerUpdate_TextChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.removeProduct);
            this.tabPage1.Controls.Add(this.removeProvider);
            this.tabPage1.Controls.Add(this.removeCostumer);
            this.tabPage1.Controls.Add(this.productsCb);
            this.tabPage1.Controls.Add(this.providersCb);
            this.tabPage1.Controls.Add(this.costumerCb);
            this.tabPage1.Controls.Add(this.deleteProductBtn);
            this.tabPage1.Controls.Add(this.deleteproviderBtn);
            this.tabPage1.Controls.Add(this.deleteCostumerBtn);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(797, 422);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // removeProduct
            // 
            this.removeProduct.Location = new System.Drawing.Point(532, 322);
            this.removeProduct.Name = "removeProduct";
            this.removeProduct.Size = new System.Drawing.Size(148, 23);
            this.removeProduct.TabIndex = 8;
            this.removeProduct.Text = "Remove the chosen product";
            this.removeProduct.UseVisualStyleBackColor = true;
            this.removeProduct.Click += new System.EventHandler(this.removeProduct_Click);
            // 
            // removeProvider
            // 
            this.removeProvider.Location = new System.Drawing.Point(270, 322);
            this.removeProvider.Name = "removeProvider";
            this.removeProvider.Size = new System.Drawing.Size(159, 23);
            this.removeProvider.TabIndex = 7;
            this.removeProvider.Text = "Remove the chosen provider";
            this.removeProvider.UseVisualStyleBackColor = true;
            this.removeProvider.Click += new System.EventHandler(this.removeProvider_Click);
            // 
            // removeCostumer
            // 
            this.removeCostumer.Location = new System.Drawing.Point(8, 322);
            this.removeCostumer.Name = "removeCostumer";
            this.removeCostumer.Size = new System.Drawing.Size(190, 23);
            this.removeCostumer.TabIndex = 6;
            this.removeCostumer.Text = "Remove the chosen costumer";
            this.removeCostumer.UseVisualStyleBackColor = true;
            this.removeCostumer.Click += new System.EventHandler(this.removeCostumer_Click);
            // 
            // productsCb
            // 
            this.productsCb.FormattingEnabled = true;
            this.productsCb.Location = new System.Drawing.Point(543, 161);
            this.productsCb.Name = "productsCb";
            this.productsCb.Size = new System.Drawing.Size(121, 21);
            this.productsCb.TabIndex = 5;
            // 
            // providersCb
            // 
            this.providersCb.FormattingEnabled = true;
            this.providersCb.Location = new System.Drawing.Point(288, 161);
            this.providersCb.Name = "providersCb";
            this.providersCb.Size = new System.Drawing.Size(121, 21);
            this.providersCb.TabIndex = 4;
            // 
            // costumerCb
            // 
            this.costumerCb.FormattingEnabled = true;
            this.costumerCb.Location = new System.Drawing.Point(6, 161);
            this.costumerCb.Name = "costumerCb";
            this.costumerCb.Size = new System.Drawing.Size(192, 21);
            this.costumerCb.TabIndex = 3;
            this.costumerCb.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // deleteProductBtn
            // 
            this.deleteProductBtn.Location = new System.Drawing.Point(532, 52);
            this.deleteProductBtn.Name = "deleteProductBtn";
            this.deleteProductBtn.Size = new System.Drawing.Size(179, 23);
            this.deleteProductBtn.TabIndex = 2;
            this.deleteProductBtn.Text = "Press here to show products";
            this.deleteProductBtn.UseVisualStyleBackColor = true;
            this.deleteProductBtn.Click += new System.EventHandler(this.deleteProductBtn_Click);
            // 
            // deleteproviderBtn
            // 
            this.deleteproviderBtn.Location = new System.Drawing.Point(270, 52);
            this.deleteproviderBtn.Name = "deleteproviderBtn";
            this.deleteproviderBtn.Size = new System.Drawing.Size(181, 23);
            this.deleteproviderBtn.TabIndex = 1;
            this.deleteproviderBtn.Text = "Press here to show providers";
            this.deleteproviderBtn.UseVisualStyleBackColor = true;
            this.deleteproviderBtn.Click += new System.EventHandler(this.deleteproviderBtn_Click);
            // 
            // deleteCostumerBtn
            // 
            this.deleteCostumerBtn.Location = new System.Drawing.Point(6, 52);
            this.deleteCostumerBtn.Name = "deleteCostumerBtn";
            this.deleteCostumerBtn.Size = new System.Drawing.Size(213, 23);
            this.deleteCostumerBtn.TabIndex = 0;
            this.deleteCostumerBtn.Text = "Press here to show costumers";
            this.deleteCostumerBtn.UseVisualStyleBackColor = true;
            this.deleteCostumerBtn.Click += new System.EventHandler(this.deleteCostumerBtn_Click);
            // 
            // MainProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.userTabControl);
            this.Name = "MainProgram";
            this.Text = "Main Program";
            this.Load += new System.EventHandler(this.MainProgram_Load);
            this.userTabControl.ResumeLayout(false);
            this.tabHome.ResumeLayout(false);
            this.tabHome.PerformLayout();
            this.tabSearch.ResumeLayout(false);
            this.tabOrder.ResumeLayout(false);
            this.tabReport.ResumeLayout(false);
            this.tabUpdate.ResumeLayout(false);
            this.tabUpdate.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl userTabControl;
        private System.Windows.Forms.TabPage tabHome;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.TabPage tabOrder;
        private System.Windows.Forms.TabPage tabReport;
        private System.Windows.Forms.ComboBox cbSearchOrdersByCostumer;
        private System.Windows.Forms.ComboBox cbProductSearchByprovider;
        private System.Windows.Forms.ComboBox cbCostumerOrder;
        private System.Windows.Forms.ComboBox cbProductOrder;
        private System.Windows.Forms.ComboBox cbProviderOrder;
        private System.Windows.Forms.ComboBox cbProductsReport;
        private System.Windows.Forms.ComboBox cbProvidersReport;
        private System.Windows.Forms.Button btnProductsReport;
        private System.Windows.Forms.Button btnProvidersReport;
        private System.Windows.Forms.Button btnCostumersReport;
        private System.Windows.Forms.ComboBox cbCostumerReport;
        private System.Windows.Forms.TabPage tabUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cityCostumerUpdate;
        private System.Windows.Forms.TextBox pnCostumerUpdate;
        private System.Windows.Forms.TextBox lnCostumerUpdate;
        private System.Windows.Forms.TextBox fnCostumerUpdate;
        private System.Windows.Forms.Button UpdateCostumerBtn;
        private System.Windows.Forms.TextBox snCostumerUpdate;
        private System.Windows.Forms.TextBox fnProviderUpdate;
        private System.Windows.Forms.TextBox amProviderUpdate;
        private System.Windows.Forms.TextBox ctProviderUpdate;
        private System.Windows.Forms.TextBox pnProviderUpdate;
        private System.Windows.Forms.TextBox lnProviderUpdate;
        private System.Windows.Forms.Button UpdateProviderBtn;
        private System.Windows.Forms.TextBox priceProduct;
        private System.Windows.Forms.TextBox aProduct;
        private System.Windows.Forms.TextBox sProduct;
        private System.Windows.Forms.Button UpdateProductBtn;
        private System.Windows.Forms.TextBox Nproduct;
        private System.Windows.Forms.TextBox cProduct;
        private System.Windows.Forms.Label lbMainPage;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox costumerCb;
        private System.Windows.Forms.Button deleteProductBtn;
        private System.Windows.Forms.Button deleteproviderBtn;
        private System.Windows.Forms.Button deleteCostumerBtn;
        private System.Windows.Forms.ComboBox productsCb;
        private System.Windows.Forms.ComboBox providersCb;
        private System.Windows.Forms.Button removeProduct;
        private System.Windows.Forms.Button removeProvider;
        private System.Windows.Forms.Button removeCostumer;
    }
}

