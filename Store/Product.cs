﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Authors:        Shoval  Eliav
///                 Nir     Spindel
/// File name:      Product.cs
/// 
/// This file for Class Product
/// </summary>
namespace Store
{
    public class Product
    {
        /// <summary>
        /// Attributes of Product with Getters & Setters
        /// ------------------------------------------------------
        /// @param ID       :   int         -   ID of product
        /// @param Name     :   string      -   Name of product
        /// @param Amount   :   string      -   Amount of product
        /// @param Size     :   string      -   Size of product
        /// @param Category :   string      -   Category of product
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string Category { get; set; }
        public string Amount { get; set; }
        public string price_per_unit { get; set; }


        /// <summary>
        /// Constructor of Product
        /// ---------------------------------
        /// </summary>
        public Product()
        {
            ID = -1;
            Name = string.Empty;
            Size = string.Empty;
            Category = string.Empty;
            Amount = string.Empty;
            price_per_unit = string.Empty;
        }


        //shovel to handle
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", this.ID, this.Name, this.Amount, this.Size, this.Category);
        }
    }

}
