﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/// <summary>
/// Authors:        Shoval  Eliav
///                 Nir     Spindel
/// File name:      DBSQL.cs
/// 
/// This file for querys
/// </summary>
namespace Store
{
    class DBSQL : DbAccess
    {
        public DBSQL(string connectionString) : base(connectionString)
        {

        }
        /// <summary>
        /// get all costumers query
        /// </summary>
        public List <Costumer> getCostumers()
        {
            DataSet ds = new DataSet();
            List <Costumer> costumers = new List<Costumer>();
            string cmdStr = "SELECT * FROM [CostumerT]";  // prepare the query


            using (OleDbCommand command = new OleDbCommand(cmdStr)) // execute the query
            {
                ds = getMultipleQuery(command);
            }

            DataTable dt = new DataTable(); // get the info out of data set object
            try
            {
            dt = ds.Tables[0];
            }
            catch { return null; }
           
            foreach (DataRow tType in dt.Rows) // import the details from data row tabke to related object
            {
                Costumer cs = new Costumer();
                cs.ID = int.Parse(tType[0].ToString());
                cs.FirstName = tType[1].ToString();
                cs.LastName = tType[2].ToString();
                cs.PhoneNumber = tType[3].ToString();
                cs.StoreName = tType[4].ToString();
                cs.City = tType[5].ToString();
                costumers.Add(cs); // add to costumer list
            }

            return costumers; // return list of costumers from DB 
            
        }

        public List <Provider> getProviders()
        {
            DataSet ds = new DataSet();
            List<Provider> providers = new List<Provider>();
            string cmdStr = "SELECT * FROM [ProviderT]";  //

            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                ds = getMultipleQuery(command);
            }

            DataTable dt = new DataTable();
            try
            {
                dt = ds.Tables[0];
            }
            catch { return null; }

            foreach (DataRow tType in dt.Rows)
            {
                Provider p = new Provider();
                p.ID = int.Parse(tType[0].ToString());
                p.FirstName = tType[1].ToString();
                p.LastName = tType[2].ToString();
                providers.Add(p);
            }

            return providers;

        }


        public List<Product> getProducts()
        {
            DataSet ds = new DataSet();
            List<Product> products = new List<Product>();
            string cmdStr = "SELECT * FROM [ProductT]";  //

            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                ds = getMultipleQuery(command);
            }

            DataTable dt = new DataTable();
            try
            {
                dt = ds.Tables[0];
            }
            catch { return null; }

            foreach (DataRow tType in dt.Rows)
            {
                Product p = new Product();
                p.ID = int.Parse(tType[0].ToString());
                p.Name = tType[1].ToString();
                p.Amount = tType[2].ToString();
                p.Size = tType[3].ToString();
                p.Category = tType[4].ToString();
                products.Add(p);
            }
            return products;
        }


        // insert costumer
        public void addCostumer(string fn, string ln, string pn, string sn, string city)
        {
            //prepare the query
            string cmdStr = "INSERT INTO [CostumerT] ([FirstName], [LastName], [PhoneNumber], [StoreName], [City]) VALUES (@fn, @ln, @pn, @sn, @city)";

            //pass the values & execute the quer
            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@fn", fn);
                command.Parameters.AddWithValue("@ln", ln);
                command.Parameters.AddWithValue("@pn", pn);
                command.Parameters.AddWithValue("@sn", sn);
                command.Parameters.AddWithValue("@city", city);
                ExecuteSimpleQuery(command);
            }
        }

        //insert provider
        public void addProvider(string fn, string ln, string pn, string ct, string am)
        {
            //prepare the query
            string cmdStr = "INSERT INTO [ProviderT] ([FirstName], [LastName], [PhoneNumber], [Category], [Amount]) VALUES (@fn, @ln, @pn, @ct, @am)";

            //pass the values & execute the quer
            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@fn", fn);
                command.Parameters.AddWithValue("@ln", ln);
                command.Parameters.AddWithValue("@pn", pn);
                command.Parameters.AddWithValue("@ct", ct);
                command.Parameters.AddWithValue("@am", am);
                ExecuteSimpleQuery(command);
            }
        }

        //insert product
        public void addProduct(string n, string s, string c, string a, string p)
        {
            //prepare the query
            string cmdStr = "INSERT INTO [ProductT] ([Name], [Size], [Category], [Amount], [Price]) VALUES (@n, @s, @c, @a, @p)";

            //pass the values & execute the quer
            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@n", n);
                command.Parameters.AddWithValue("@s", s);
                command.Parameters.AddWithValue("@c", c);
                command.Parameters.AddWithValue("@a", a);
                command.Parameters.AddWithValue("@p", p);
                ExecuteSimpleQuery(command);
            }
        }


        public void deleteCostumer(string toDelete)
        {
            string nameToDelete = toDelete.Split(' ').Last();
            string cmdStr = "DELETE * " + "FROM [CostumerT] " + "WHERE LastName=@LastName";

            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@LastName", nameToDelete);
                ExecuteSimpleQuery(command);
            }

        }

        public void deleteProvider(string toDelete)
        {
            string nameToDelete = toDelete.Split(' ').Last();
            string cmdStr = "DELETE * " + "FROM [ProviderT] " + "WHERE LastName=@LastName";

            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@LastName", nameToDelete);
                ExecuteSimpleQuery(command);
            }

        }

        public void deleteProduct(string toDelete)
        {
            string nameToDelete = toDelete.Split(' ').Last();
            string cmdStr = "DELETE * " + "FROM [ProductT] " + "WHERE Name=@Name";

            using (OleDbCommand command = new OleDbCommand(cmdStr))
            {
                command.Parameters.AddWithValue("@Name", nameToDelete);
                ExecuteSimpleQuery(command);
            }

        }

    }
}
