﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/// <summary>
/// Authors:        Shoval  Eliav
///                 Nir     Spindel
/// File name:      Main.cs
/// 
/// This file for main program
/// </summary>
namespace Store
{
    public partial class MainProgram : Form
    {
        public MainProgram()
        {
            InitializeComponent();
        }
        DBSQL db;//Global DB instance for connection to DataBase
        private void MainProgram_Load(object sender, EventArgs e)
        {
            db = new DBSQL(@"Provider=Microsoft.ACE.OLEDB.12.0;
                                Data Source=D:\latest\c-final\Store_Database.accdb;
                                Persist Security Info=False;");
        }
        

        
        //Fucntion to generate costumers report
        private void btnCostumersReport_Click(object sender, EventArgs e)
        {
                //get all costumers data from the DB
                List<Costumer> costumers = db.getCostumers();
                //create new file to wrote to
                TextWriter tw = new StreamWriter("D:\\Costumers_Report.txt");
                //write the database list to file
                foreach (Costumer c in costumers)
                    tw.WriteLine(c);
                tw.Close();
                MessageBox.Show("Costumers report has been generated"); 
        }

        //Fucntion to generate providers report
        private void btnProvidersReport_Click(object sender, EventArgs e)
        {
            //get all providers data from the DB
            List<Provider> providers = db.getProviders();
            //create new file to wrote to
            TextWriter tw = new StreamWriter("D:\\Providers_Report.txt");
            //write the database list to file
            foreach (Provider p in providers)
                tw.WriteLine(p);
            tw.Close();
            MessageBox.Show("Providers report has been generated");
        }

        //Fucntion to generate products report
        private void btnProductsReport_Click(object sender, EventArgs e)
        {
        //get all costumers data from the DB
        List<Product> products = db.getProducts();
        //create new file to wrote to
        TextWriter tw = new StreamWriter("D:\\Products_Report.txt");
        //write the database list to file
        foreach (Product p in products)
            tw.WriteLine(p);
        tw.Close();
        MessageBox.Show("Products report has been generated");
    }

        
        private void UpdateCostumerBtn_Click(object sender, EventArgs e)
        {
          
            if (CheckUpdateFieldsIsEmpty(fnCostumerUpdate.Text, lnCostumerUpdate.Text, pnCostumerUpdate.Text, snCostumerUpdate.Text, cityCostumerUpdate.Text))
            {
                MessageBox.Show("Please fill all fields!");
       

                
            
            }
            
            if (true)
            {
                MessageBox.Show("Costumer updated!\n"
                                    + "First name: " + fnCostumerUpdate.Text + "\n"
                                    + "Last name: " + lnCostumerUpdate.Text + "\n"
                                    + "Phone number: " + pnCostumerUpdate.Text + "\n"
                                    + "Store name: " + snCostumerUpdate.Text + "\n"
                                    + "City: " + cityCostumerUpdate.Text
                               );
                db.addCostumer(fnCostumerUpdate.Text, lnCostumerUpdate.Text, pnCostumerUpdate.Text, snCostumerUpdate.Text, cityCostumerUpdate.Text);
            }
        }
        private void first_name_enter_text(object sender, EventArgs e)
        {
            if(fnCostumerUpdate.Text=="First Name")
            {
                fnCostumerUpdate.Text = "";
            }
        }
        private void first_name_leave_text(object sender, EventArgs e)
        {
            if (fnCostumerUpdate.Text == "")
            {
                fnCostumerUpdate.Text = "First Name";
            }
        }
        private void UpdateProviderBtn_Click(object sender, EventArgs e)
        {
            bool flag = true;
            if (CheckUpdateFieldsIsEmpty(fnProviderUpdate.Text, lnProviderUpdate.Text, pnProviderUpdate.Text, ctProviderUpdate.Text, amProviderUpdate.Text))
            {
                MessageBox.Show("Please fill all fields!");
              
            }
           
            if (flag)
            {
                MessageBox.Show("Costumer updated!\n"
                                    + "First name: " + fnProviderUpdate.Text + "\n"
                                    + "Last name: " + lnProviderUpdate.Text + "\n"
                                    + "Phone number: " + pnProviderUpdate.Text + "\n"
                                    + "Store name: " + ctProviderUpdate.Text + "\n"
                                    + "Amount: " + amProviderUpdate.Text
                               );
                db.addCostumer(fnProviderUpdate.Text, lnProviderUpdate.Text, pnProviderUpdate.Text, ctProviderUpdate.Text, amProviderUpdate.Text);
            }
        }
    
        private void UpdateProductBtn_Click(object sender, EventArgs e)
        {
            bool flag = true;
            if (CheckUpdateFieldsIsEmpty(Nproduct.Text, sProduct.Text, cProduct.Text, aProduct.Text, priceProduct.Text))
            {
                MessageBox.Show("Please fill all fields!");
                flag = false;
            }
            
            if (flag)
            {
                MessageBox.Show("Costumer updated!\n"
                                    + "Name: " + Nproduct.Text + "\n"
                                    + "Size: " + sProduct.Text + "\n"
                                    + "Category: " + cProduct.Text + "\n"
                                    + "Amount: " + aProduct.Text + "\n"
                                    + "Price: " + priceProduct.Text
                               );
                db.addCostumer(Nproduct.Text, sProduct.Text, cProduct.Text, aProduct.Text, priceProduct.Text);
            }
        }
        
        
        /// <summary>
        /// Bool function to check if fields in update tab are emptys! 
        /// </summary>
        /// <param name="str"></param>
        /// <returns>
        ///     return true if field are empty
        /// </returns>
        private bool CheckUpdateFieldsIsEmpty(string str1, string str2, string str3, string str4, string str5)
        {
            if (str1 != "" && str2 != "" && str3 != "" && str4 != "" && str5 != "")
                return false;
            return true;
        }
        

    private void deleteCostumerBtn_Click(object sender, EventArgs e)
        {
            //get all costumers data from the DB
            List<Costumer> costumers = db.getCostumers();

            //populate the combobox from the list
            foreach (Costumer cs in costumers)
                costumerCb.Items.Add(cs.FirstName +" "+ cs.LastName);
            //get all costumers data from the DB//get all costumers data from the DB//get all costumers data from the DB
            //get all costumers data from the DB//get all costumers data from the DB//get all costumers data from the DB
            //disable the button after one press
            deleteCostumerBtn.Enabled = false;

            //set the first item as defualt
            costumerCb.SelectedIndex = 0;
        }

        private void deleteproviderBtn_Click(object sender, EventArgs e)
        {
            //get all providers data from the DB
            List<Provider> providers = db.getProviders();

            //populate the combobox from the list
            foreach (Provider p in providers)
                providersCb.Items.Add(p.FirstName +" "+ p.LastName);
            //populate the combobox from the list
            //populate the combobox from the list
            //populate the combobox from the list
            //populate the combobox from the list
            //populate the combobox from the list
            //populate the combobox from the list
            //disable the button after one press
            deleteproviderBtn.Enabled = false;

            //set the first item as defualt
            providersCb.SelectedIndex = 0;
        }

        private void deleteProductBtn_Click(object sender, EventArgs e)
        {
            //get all products data from the DB
            List<Product> products = db.getProducts();

            //populate the combobox from the list
            foreach (Product p in products)
                productsCb.Items.Add(p.Name);

            //disable the button after one press
            deleteProductBtn.Enabled = false;

            //set the first item as defualt
            productsCb.SelectedIndex = 0;
        }

        private void removeCostumer_Click(object sender, EventArgs e)
        {
             db.deleteCostumer(costumerCb.Text);
             deleteCostumerBtn.Enabled = true;
             costumerCb.Items.Clear();
             deleteCostumerBtn.PerformClick();

        }

        private void removeProvider_Click(object sender, EventArgs e)
        {
            db.deleteProvider(providersCb.Text);
            deleteproviderBtn.Enabled = true;
            providersCb.Items.Clear();
            deleteproviderBtn.PerformClick();
        }

        private void removeProduct_Click(object sender, EventArgs e)
        {
            db.deleteProduct(productsCb.Text);
            deleteProductBtn.Enabled = true;
            productsCb.Items.Clear();
            deleteProductBtn.PerformClick();
        }

       
    }
}
